import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {ZahteviService} from '../zahtevi.service';

@Component({
  selector: 'zahtevi-container',
  templateUrl: './zahtevi.component.html',
  styleUrls: ['./zahtevi.component.css']
})

export class ZahteviComponent implements OnInit{
    zahtevi = {};
    zahtevi_a = {};
    zahtevi_d = {};
    private pocetnaCena: number;
    private RsdPoKm: number;
    private kilometraza: number;
    constructor(private  zahteviService: ZahteviService){}

    ngOnInit(){
        this.ucitajZahteve();
    }

    sacuvajZahtev(polaziste, odrediste,stranka,vozilo,trajanje){
            if(polaziste == null || polaziste == ""){
                alert("Unesite polaziste!");
            } else if (odrediste == null || odrediste == ""){
                alert("Unesite odrediste!");
            } else if (stranka == null || stranka == ""){
                alert("Unesite stranku!");
            } else if (vozilo == null || vozilo == ""){
                alert("Unesite vozilo!");
            } else if (trajanje == null || trajanje == ""){
                alert("Unesite trajanje!")
            } else {
                    this.kilometraza = trajanje;
                    this.zahteviService.skladistiZahteve({
                        id: this.generisiId(),
                        polaziste: polaziste,
                        odrediste: odrediste,
                        stranka: stranka,
                        vozilo: vozilo,
                        korisnik: "Petar Petrović",
                        cena: this.generisiCenu(),
                        trajanje:trajanje,
                        status: "poziv"})
                        .subscribe((response) => console.log(response),
                            (error) => console.log(error));
                    this.ucitajZahteve();
            }

    }
    ucitajZahteve(){
      this.zahteviService.izlistavanjeZahteva()
          .subscribe(
              (zahtevi: any) => this.zahtevi = zahtevi,
              (error) => console.log(error));
    }
    preuzmiZahtev(id_d, polaziste_d, odrediste_d, stranka_d, vozilo_d, trajanje_d){
        this.zahteviService.preuzimanjeZahteva(id_d)
            .subscribe(
                (zahtevi_d: any) => this.zahtevi_d = zahtevi_d,
                (error) => console.log(error));
    }

    obrisiZahtev(id){
        this.zahteviService.brisanjeZahteva(id).subscribe(
            (zahtevi: any) => this.zahtevi = zahtevi,
            (error) => console.log(error));
        this.ucitajZahteve()
    }

    azurirajZahtev(id_a, polaziste_a, odrediste_a,stranka_a,vozilo_a,trajanje_a) {
        if(id_a == null || id_a == ""){
            alert("Unesite id zahteva koji zelite da azurirate!");
        }else if (polaziste_a == null || polaziste_a == ""){
            alert("Unesite polaziste!");
        } else if (odrediste_a == null || odrediste_a == ""){
            alert("Unesite odrediste!");
        } else if (stranka_a == null || stranka_a == ""){
            alert("Unesite stranku!");
        } else if (vozilo_a == null || vozilo_a == ""){
            alert("Unesite vozilo!");
        } else if (trajanje_a == null || trajanje_a == ""){
            alert("Unesite trajanje!")
        } else {
            this.kilometraza = trajanje_a;
            this.zahteviService.azurirajZahteve({
                id: id_a,
                polaziste: polaziste_a,
                odrediste: odrediste_a,
                stranka: stranka_a,
                vozilo: vozilo_a,
                korisnik: "Petar Petrović",
                cena: this.generisiCenu(),
                trajanje:trajanje_a,
                status: "poziv"})
                .subscribe((response) => console.log(response),
                    (error) => console.log(error));
            this.ucitajZahteve();
        }

    }
    private generisiId(){
        return Math.round(Math.random() * 10000);
    }
    generisiCenu(){
        this.pocetnaCena = 20;
        this.RsdPoKm = 50;
        return Math.round(this.kilometraza * this.RsdPoKm + this.pocetnaCena);
}
}
