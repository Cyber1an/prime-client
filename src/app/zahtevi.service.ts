import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class ZahteviService {
    private serverAddress = 'http://localhost:8080';
    constructor(private http: Http){}
    skladistiZahteve(zahtevi: {}){
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(this.serverAddress + '/zahtevi/dodajZahtev', zahtevi, { headers: headers });
    }

    izlistavanjeZahteva(){
        return this.http.get(this.serverAddress + '/zahtevi/izlistaj/data.json').map((response: Response) => {const data = response.json(); return data; })
            .catch((error: Response) => {
                return Observable.throw(error);
            });
    }
    preuzimanjeZahteva(id){
        return this.http.get(this.serverAddress + '/zahtevi/preuzmiZahtev/' + id).map((response: Response) => {const data = response.json(); return data; })
            .catch((error: Response) => {
                return Observable.throw(error);
            });
    }
    brisanjeZahteva(id){
        return this.http.get(this.serverAddress + '/zahtevi/obrisiZahtev/' + id);
    }

    azurirajZahteve(zahtevi_a: {}){
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put(this.serverAddress + '/zahtevi/azurirajZahtev', zahtevi_a, { headers: headers });
    }
}