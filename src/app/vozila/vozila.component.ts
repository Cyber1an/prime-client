import { Component, OnInit } from '@angular/core';
import {VozilaService} from '../vozila.service';

@Component({
  selector: 'app-vozila',
  templateUrl: './vozila.component.html',
  styleUrls: ['./vozila.component.css']
})
export class VozilaComponent implements OnInit {

    vozila = {};
    constructor(private  vozilaService: VozilaService){}

    ngOnInit(){
        this.ucitajVozila();
    }

    sacuvajVozilo(marka, model, rb, tablice, registracija, vozac, slika){
        if(marka == null || marka == ""){
            alert("Unesite marku vozila!");
        } else if (model == null || model == ""){
            alert("Unesite model vozila!");
        } else if (rb == null || rb == ""){
            alert("Unesite redni broj vozila!");
        } else if (tablice == null || tablice == ""){
            alert("Unesite Tablice!");
        } else if (registracija == null || registracija == ""){
            alert("Unesite datum isteka registracije!");
        } else if (vozac == null || vozac == ""){
            alert("Unesite vozaca");
        } else if (slika == null || slika == ""){
            alert("Unesite url slike");
        } else{
            this.vozilaService.skladistiVozilo({
                id: this.generisiId(),
                marka:marka,
                model:model,
                rb: rb,
                tablice: tablice,
                registracija: registracija,
                vozac: vozac,
                slika: slika})
                .subscribe((response) => console.log(response),
                    (error) => console.log(error));
            this.ucitajVozila();
        }
    }
    ucitajVozila(){
        this.vozilaService.izlistavanjeVozila()
            .subscribe(
                (vozila: any) => this.vozila = vozila,
                (error) => console.log(error));
    }
    obrisiVozilo(id){
        this.vozilaService.brisanjeVozila(id).subscribe(
            (vozila: any) => this.vozila = vozila,
            (error) => console.log(error));
        this.ucitajVozila();
    }
    private generisiId() {
        return Math.round(Math.random() * 10000);
    }
}
