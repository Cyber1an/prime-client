import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class VozilaService {
    private serverAddress = 'http://localhost:8080';
    constructor(private http: Http){}
    skladistiVozilo(vozila: {}){
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(this.serverAddress + '/vozila/dodajVozilo', vozila, { headers: headers });
    }

    izlistavanjeVozila(){
        return this.http.get(this.serverAddress + '/vozila/izlistaj/data.json').map((response: Response) => {const data = response.json(); return data; })
            .catch((error: Response) => {
                return Observable.throw(error);
            });
    }
    brisanjeVozila(id){
        return this.http.get(this.serverAddress + '/vozila/obrisiVozilo/' + id);
    }
}