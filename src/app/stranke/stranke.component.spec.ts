import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrankeComponent } from './stranke.component';

describe('StrankeComponent', () => {
  let component: StrankeComponent;
  let fixture: ComponentFixture<StrankeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrankeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrankeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
