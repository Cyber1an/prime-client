import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'

import { AppComponent } from './app.component';
import { ZahteviComponent } from './zahtevi/zahtevi.component';
import { HomeComponent } from './home/home.component';
import { KorisniciComponent } from './korisnici/korisnici.component';
import { VozilaComponent } from './vozila/vozila.component';
import { StrankeComponent } from './stranke/stranke.component';
import { VozaciComponent } from './vozaci/vozaci.component';
import { NavigacijaComponent } from './navigacija/navigacija.component';
import { LoginComponent } from './login/login.component';

import {VozilaService} from './vozila.service';
import {ZahteviService} from './zahtevi.service';
import { LokacijeComponent } from './lokacije/lokacije.component';

@NgModule({
  declarations: [
    AppComponent,
    ZahteviComponent,
    HomeComponent,
    KorisniciComponent,
    VozilaComponent,
    StrankeComponent,
    VozaciComponent,
    NavigacijaComponent,
    LoginComponent,
    LokacijeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'lokacije',
        component: LokacijeComponent
      },
      {
        path: 'zahtevi',
        component: ZahteviComponent
      },
      {
        path: 'korisnici',
        component: KorisniciComponent
      },
      {
        path: 'stranke',
        component: StrankeComponent
      },
      {
        path: 'vozaci',
        component: VozaciComponent,
      },
      {
        path: 'vozila',
        component: VozilaComponent
      }
    ])
  ],
  providers: [ZahteviService, VozilaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
