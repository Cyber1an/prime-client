import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VozaciComponent } from './vozaci.component';

describe('VozaciComponent', () => {
  let component: VozaciComponent;
  let fixture: ComponentFixture<VozaciComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VozaciComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VozaciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
